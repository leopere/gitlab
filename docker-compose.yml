version: '3.4'

networks:
  traefik:
    external: true

services:
#####################
## Proxy container ##
#####################
## One of the major contributing guides here.
## https://www.digitalocean.com/community/tutorials/how-to-use-traefik-as-a-reverse-proxy-for-docker-containers-on-ubuntu-16-04
  proxy:
    image: traefik:alpine
    command: --api --docker --docker.domain=docker.localhost --logLevel=DEBUG
    networks:
      traefik:
        ipv4_address: 10.5.0.100
    ports:
      - target: 80
        published: 80
        mode: host
      - target: 443
        published: 443
        mode: host
    expose:
      - 8080
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - ./traefik.toml:/traefik.toml
      ## This is going to be stored by default in the following host dir in staging.
      #  There is a rate limit for how many certificate issuances you can request per week.
      #  Because of this we will store the acme.json file persistently.
      - ./traefik/:/opt/traefik/
    labels:
      # https://docs.traefik.io/user-guide/docker-and-lets-encrypt/
      - "traefik.backend=proxy"
      - "traefik.docker.network=traefik"
      - "traefik.frontend.rule=Host:monitor.YOURDOMAIN.HERE"
      - "traefik.expose=true"
      - "traefik.port=8080"
    healthcheck:
       test: ["CMD", "traefik", "healthcheck"]
       timeout: 10s
       retries: 3

#####################
## Redis Container ##
#####################
  redis:
    image: sameersbn/redis:4.0.9-1
    restart: always
    labels:
      - traefik.enable=false
    command:
    - --loglevel warning
    # - --protected-mode no
    volumes:
    - ./data/redis:/var/lib/redis:Z
    networks:
      traefik:
        ipv4_address: 10.5.0.62

########################
## Postgres Container ##
########################
  postgresql:
    image: sameersbn/postgresql:10
    restart: always
    environment:
    - DB_USER=gitlab
    - DB_PASS=REPLACEMEWITHSOMETHINGSUPERSECURELATER
    - DB_NAME=gitlabhq_production
    - DB_EXTENSION=pg_trgm
    volumes:
      - ./data/postgresql:/var/lib/postgresql:Z
    labels:
      - traefik.enable=false
    networks:
      traefik:
        ipv4_address: 10.5.0.61

############
## GitLab ##
############
  gitlab:
    ## Something other than this
    ## https://github.com/sameersbn/docker-gitlab/blob/master/docker-compose.yml
    ## Documentation
    ## https://hub.docker.com/r/sameersbn/gitlab/

    image: sameersbn/gitlab:11.1.3
    depends_on:
      - redis
      - postgresql
    restart: always
    labels:
      # https://docs.traefik.io/user-guide/docker-and-lets-encrypt/
      - "traefik.gitlab.backend=gitlab"
      - "traefik.registry.backend=registry"
      - "traefik.docker.net work=traefik"
      - "traefik.gitlab.frontend.rule=Host:kwlug.nixc.us"
      #- "traefik.registry.frontend.rule=Host:registry.YOURDOMAIN.HERE"
      - "traefik.gitlab.expose=true"
      #- "traefik.registry.expose=true"
      - "traefik.gitlab.port=80"
      #- "traefik.registry.port=8181"

    expose:
      ## Expose for Traefik
      - '80'
      - '8181'
    ports:
      ## Port bind for git
      - '10022:22'
    networks:
      traefik:
        ipv4_address: 10.5.0.60
    volumes:
    - ./data/gitlab:/home/git/data:Z
    environment:
    - DEBUG=false

    - DB_ADAPTER=postgresql
    - DB_HOST=postgresql
    - DB_PORT=5432
    - DB_USER=gitlab
    - DB_PASS=REPLACEMEWITHSOMETHINGSUPERSECURELATER
    - DB_NAME=gitlabhq_production

    - REDIS_HOST=redis
    - REDIS_PORT=6379

    ## Update TimeZone for your local

    - TZ=Asia/Kolkata
    - GITLAB_TIMEZONE=Kolkata

    ## Must be disabled for reverse proxies like Traefik to work.

    - GITLAB_HTTPS=false
    - SSL_SELF_SIGNED=false

    ## Update these values

    - GITLAB_HOST=localhost
    - GITLAB_PORT=10080
    - GITLAB_SSH_PORT=10022
    - GITLAB_RELATIVE_URL_ROOT=
    - GITLAB_SECRETS_DB_KEY_BASE=REPLACEMEWITHSOMETHINGSUPERSECURELATER
    - GITLAB_SECRETS_SECRET_KEY_BASE=REPLACEMEWITHSOMETHINGSUPERSECURELATER
    - GITLAB_SECRETS_OTP_KEY_BASE=REPLACEMEWITHSOMETHINGSUPERSECURELATER

    ## This must be set for you to be able to log in on first use.
    ## If you would rather not use this insecure method of configuring the user then omit them in production.

    - GITLAB_ROOT_PASSWORD=password
    - GITLAB_ROOT_EMAIL=user@example.com

    - GITLAB_NOTIFY_ON_BROKEN_BUILDS=true
    - GITLAB_NOTIFY_PUSHER=false

    - GITLAB_EMAIL=notifications@example.com
    - GITLAB_EMAIL_REPLY_TO=noreply@example.com
    - GITLAB_INCOMING_EMAIL_ADDRESS=reply@example.com

    - GITLAB_BACKUP_SCHEDULE=daily
    - GITLAB_BACKUP_TIME=01:00

    ## Email is important for a lot of parts of GitLab so it's highly recommended but not required.

    - SMTP_ENABLED=false
    - SMTP_DOMAIN=www.example.com
    - SMTP_HOST=smtp.gmail.com
    - SMTP_PORT=587
    - SMTP_USER=mailer@example.com
    - SMTP_PASS=password
    - SMTP_STARTTLS=true
    - SMTP_AUTHENTICATION=login

    - IMAP_ENABLED=false
    - IMAP_HOST=imap.gmail.com
    - IMAP_PORT=993
    - IMAP_USER=mailer@example.com
    - IMAP_PASS=password
    - IMAP_SSL=true
    - IMAP_STARTTLS=false

    ## This section is important if you want something for hosting your own private Docker Registry via GitLab
    ## I Highly recommend that you take the time to learn how to do this.

    # - GITLAB_REGISTRY_ENABLED=true      # Enables the GitLab Container Registry. Defaults to false.
    # - GITLAB_REGISTRY_HOST=registry.YOURDOMAIN.HERE       # Sets the GitLab Registry Host. Defaults to registry.example.com
    # - GITLAB_REGISTRY_PORT=8181       # Sets the GitLab Registry Port. Defaults to 443.
    # - GITLAB_REGISTRY_API_URL=      # Sets the GitLab Registry API URL. Defaults to http://localhost:5000
    # - GITLAB_REGISTRY_KEY_PATH=       # Sets the GitLab Registry Key Path. Defaults to config/registry.key
    # - GITLAB_REGISTRY_DIR=      # Directory to store the container images will be shared with registry. Defaults to $GITLAB_SHARED_DIR/registry
    # - GITLAB_REGISTRY_ISSUER=       # Sets the GitLab Registry Issuer. Defaults to gitlab-issuer.

    - OAUTH_ENABLED=false
    - OAUTH_AUTO_SIGN_IN_WITH_PROVIDER=
    - OAUTH_ALLOW_SSO=
    - OAUTH_BLOCK_AUTO_CREATED_USERS=true
    - OAUTH_AUTO_LINK_LDAP_USER=false
    - OAUTH_AUTO_LINK_SAML_USER=false
    - OAUTH_EXTERNAL_PROVIDERS=

    - OAUTH_CAS3_LABEL=cas3
    - OAUTH_CAS3_SERVER=
    - OAUTH_CAS3_DISABLE_SSL_VERIFICATION=false
    - OAUTH_CAS3_LOGIN_URL=/cas/login
    - OAUTH_CAS3_VALIDATE_URL=/cas/p3/serviceValidate
    - OAUTH_CAS3_LOGOUT_URL=/cas/logout

    - OAUTH_GOOGLE_API_KEY=
    - OAUTH_GOOGLE_APP_SECRET=
    - OAUTH_GOOGLE_RESTRICT_DOMAIN=

    - OAUTH_FACEBOOK_API_KEY=
    - OAUTH_FACEBOOK_APP_SECRET=

    - OAUTH_TWITTER_API_KEY=
    - OAUTH_TWITTER_APP_SECRET=

    - OAUTH_GITHUB_API_KEY=
    - OAUTH_GITHUB_APP_SECRET=
    - OAUTH_GITHUB_URL=
    - OAUTH_GITHUB_VERIFY_SSL=

    - OAUTH_GITLAB_API_KEY=
    - OAUTH_GITLAB_APP_SECRET=

    - OAUTH_BITBUCKET_API_KEY=
    - OAUTH_BITBUCKET_APP_SECRET=

    - OAUTH_SAML_ASSERTION_CONSUMER_SERVICE_URL=
    - OAUTH_SAML_IDP_CERT_FINGERPRINT=
    - OAUTH_SAML_IDP_SSO_TARGET_URL=
    - OAUTH_SAML_ISSUER=
    - OAUTH_SAML_LABEL="Our SAML Provider"
    - OAUTH_SAML_NAME_IDENTIFIER_FORMAT=urn:oasis:names:tc:SAML:2.0:nameid-format:transient
    - OAUTH_SAML_GROUPS_ATTRIBUTE=
    - OAUTH_SAML_EXTERNAL_GROUPS=
    - OAUTH_SAML_ATTRIBUTE_STATEMENTS_EMAIL=
    - OAUTH_SAML_ATTRIBUTE_STATEMENTS_NAME=
    - OAUTH_SAML_ATTRIBUTE_STATEMENTS_FIRST_NAME=
    - OAUTH_SAML_ATTRIBUTE_STATEMENTS_LAST_NAME=

    - OAUTH_CROWD_SERVER_URL=
    - OAUTH_CROWD_APP_NAME=
    - OAUTH_CROWD_APP_PASSWORD=

    - OAUTH_AUTH0_CLIENT_ID=
    - OAUTH_AUTH0_CLIENT_SECRET=
    - OAUTH_AUTH0_DOMAIN=

    - OAUTH_AZURE_API_KEY=
    - OAUTH_AZURE_API_SECRET=
    - OAUTH_AZURE_TENANT_ID=
